def add(a, b):
    """
    Function to add two numbers.
    
    Args:
        a (int): First number.
        b (int): Second number.
    
    Returns:
        int: The sum of a and b.
    """
    return a + b

def multiply(a, b):
    """
    Function to multiply two numbers.
    
    Args:
        a (int): First number.
        b (int): Second number.
    
    Returns:
        int: The product of a and b.
    """
    return a * b

# Example usage:
num1 = 5
num2 = 3

print("Adding:", add(num1, num2))
print("Multiplying:", multiply(num1, num2))

